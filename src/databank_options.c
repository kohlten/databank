//
// Created by kohlten on 1/30/19.
//

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

#include "databank_options.h"
#include "databank_util.h"
#include "databank_memory.h"
#include "databank_hashtable.h"
#include "databank_io.h"

static int check_files(char **files, uint32_t files_len, t_databank_options *options) {
    t_hashtable hashtable;
    hashtable_error err;
    char **new_files;
    char *file_name;
    uint32_t i, j;

    init_hashtable(&hashtable, files_len);
    new_files = databank_malloc((files_len + 1) * sizeof(char *));
    if (!new_files) {
        error("Out of memory!", false, false);
        return -1;
    }
    for (i = 0, j = 0; i < files_len; i++) {
        if (is_directory(files[i]) == true) {
            printf("WARNING: %s is a directory!\n", files[i]);
            continue;
        }
        if (file_exists(files[i]) == false) {
            fprintf(stderr, "File %s does not exist!\n", files[i]);
            free(new_files);
            return -1;
        }
        file_name = get_file_name(files[i]);
        if (!file_name) {
            free(new_files);
            free(file_name);
            return error("Out of memory!", false, false);
        }
        err = add_node_hashtable(&hashtable, file_name, &i);
        if (err == HASH_DUPLICATE_DATA) {
            printf("WARNING: File %s is already in the files!\n", files[i]);
            free(file_name);
            continue;
        } else if (err != HASH_SUCCESS) {
            free(new_files);
            free(file_name);
            return error("Failed to add node to hashtable!\n", false, false);;
        }
        new_files[j] = strdup(files[i]);
        if (!new_files[j]) {
            free(new_files);
            free(file_name);
            return error("Out of memory!", false, false);;
        }
        free(file_name);
        j++;
    }
    free_hashtable(&hashtable, false, NULL);
    if (j == 0) {
        free(new_files);
        return error("No files!", false, true);
    }
    options->files = new_files;
    options->nodes = j;
    return 0;
}

static int parse_arguments(int argc, char **argv, t_databank_options *options) {
    int i;

    i = 1;
    while (i < argc) {
        if (argv[i][0] == '-') {
            if (strcmp(argv[i], "-a") == 0 || strcmp(argv[i], "--algorithm") == 0) {
                if (i + 1 >= argc)
                    return error("Not enough arguments!", false, true);
                i++;
                if (options->compression_algorithm != NULL) {
                    printf("WARNING: Supplying multiple compression algorithms!\n");
                    free(options->compression_algorithm);
                }
                options->compression_algorithm = strdup(argv[i]);
                if (!options->compression_algorithm)
                    return error("Out of memory!", false, false);
                options->codec = squash_get_codec(options->compression_algorithm);
                if (options->codec == NULL) {
                    fprintf(stderr, "Unable to find compression algorithm %s\n", options->compression_algorithm);
                    return -1;
                }
                if (i == argc - 1) {
                    printf("Algortihm %s found!\n", options->compression_algorithm);
                    free_options(options);
                    exit(0);
                }
            } else if (strcmp(argv[i], "-d") == 0 || strcmp(argv[i], "--decompress") == 0)
                options->type = options->type | DATABANK_DECOMPRESS;
            else if (strcmp(argv[i], "-c") == 0 || strcmp(argv[i], "--compress") == 0)
                options->type = options->type | DATABANK_COMPRESS;
            else if (strcmp(argv[i], "-l") == 0 || strcmp(argv[i], "--level") == 0) {
                if (i + 1 >= argc)
                    return error("Not enough arguments!", false, true);
                i++;
                options->level = (uint32_t) atoi(argv[i]);
            }
        } else
            break;
        i++;
    }
    return i;
}

int parse_options(int argc, char **argv, t_databank_options *options) {
    int i;

    if (argc < 2)
        error("Not enough arguments!", true, true);
    if (strcmp(argv[1], "-h") == 0 || strcmp(argv[1], "--help") == 0) {
        print_help();
        exit(0);
    }
    if ((i = parse_arguments(argc, argv, options)) == -1)
        return -1;
    if (i >= argc)
        return error("Not enough arguments!", false, true);
    if ((options->type & DATABANK_DECOMPRESS) > 0 && (options->type & DATABANK_COMPRESS) > 0)
        return error("Too many operations!", false, true);
    if ((options->type & DATABANK_DECOMPRESS) == 0) {
        if (i + 1 >= argc)
            return error("Not enough arguments!", false, true);
        options->outfile = strdup(argv[i]);
        if (!options->outfile)
            return error("Out of memory!", false, false);
        i++;
    }
    if (check_files(argv + i, (uint32_t) argc - (uint32_t) i, options) != 0)
        return -1;
    if (!options->files)
        return error("Out of memory!\n", false, false);
    if (options->compression_algorithm) {
        options->codec = squash_get_codec(options->compression_algorithm);
        if (options->codec == NULL) {
            fprintf(stderr, "Unable to find compression algorithm %s\n", options->compression_algorithm);
            return -1;
        }
        if (options->type == 0)
            return error("Unknown operation type!", false, true);
    } else if (options->type == 0)
        return error("Unknown operation type!", false, true);
    return 0;
}

void free_options(t_databank_options *options) {
    if (options->compression_algorithm)
        free(options->compression_algorithm);
    if (options->files)
        free_2d_array(options->files);
    if (options->outfile)
        free(options->outfile);
}