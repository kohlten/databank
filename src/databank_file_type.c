/** @addtogroup databank
 * @{
*/

//
// Created by kohlten on 2/3/19.
//

#include <stdlib.h>
#include <stdint.h>
#include <string.h>

#include "databank_file_type.h"

/**
 * Return the type of file
 * @param type Extention of the file
 * @return DatabankType
 */
DatabankType type_of_file(char *type) {
    uint32_t i;
    char *image_formats[] = {"jpeg", "jpg",
                             "tif", "tiff",
                             "gif", "bmp",
                             "png", "ppm",
                             "pgm", "pbm",
                             "pnm", NULL};

    char *font_formats[] = {"ttf", "otf",
                            "eot", "woff",
                            "svg", NULL};

    for (i = 0; image_formats[i]; i++)
        if (strcmp(image_formats[i], type) == 0)
            return DATABANK_IMAGE;
    for (i = 0; font_formats[i]; i++)
        if (strcmp(image_formats[i], type) == 0)
            return DATABANK_FONT;
    return DATABANK_OTHER;
}

/**
 * @}
*/