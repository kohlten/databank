//
// Created by kohlten on 1/30/19.
//

#include <stdlib.h>
#include <errno.h>
#include <stdio.h>
#include <string.h>
#include <time.h>

#include "databank_create_bank.h"
#include "databank_io.h"
#include "databank_util.h"
#include "databank_memory.h"

static inline uint8_t *
create_node_header(char *node_name, uint64_t node_compressed_size,
                   uint64_t node_uncompressed_size,
                   uint64_t *header_length_p, DatabankStatus *status) {
    char *type;
    uint64_t header_length;
    uint8_t *header;

    type = get_file_type(node_name);
    if (!type) {
        *status = DATABANK_MALLOC;
        return NULL;
    }
    header_length = get_len_of_number(node_compressed_size) + get_len_of_number(node_uncompressed_size) + strlen(type) +
                    strlen(node_name) + 5;
    header = databank_malloc((header_length + 2) * sizeof(uint8_t));
    if (!header) {
        *status = DATABANK_MALLOC;
        return NULL;
    }
#ifdef __APPLE__
    snprintf((char *)header, header_length + 1, "\2%s\23%s\23%llu\23%llu\23", node_name, type, node_compressed_size, node_uncompressed_size);
#elif __linux__
    snprintf((char *) header, header_length + 1, "\2%s\23%s\23%lu\23%lu\23", node_name, type, node_compressed_size,
             node_uncompressed_size);
#endif
    *status = DATABANK_OK;
    *header_length_p = header_length;
    free(type);
    return header;
}

static inline DatabankStatus
write_node(char *node_name, SquashCodec *codec, FILE *outfile, uint32_t level, uint64_t *compressed_len,
           uint64_t *uncompressed_len) {
    char *name;
    char *level_str;
    uint32_t level_length;
    uint8_t *header;
    uint64_t header_length;
    uint8_t *text;
    uint8_t *compressed;
    uint64_t compressed_size;
    SquashStatus status;
    DatabankStatus d_status;
    uint64_t text_len;

    text = (uint8_t *) read_binary_file(node_name, &text_len);
    if (!text)
        return DATABANK_IO;
    compressed_size = squash_codec_get_max_compressed_size(codec, text_len) * 100;
    compressed = databank_malloc(compressed_size * sizeof(uint8_t));
    if (!compressed)
        return DATABANK_MALLOC;
    level_length = (uint32_t) get_len_of_number(level) + 1;
    level_str = databank_malloc(level_length * sizeof(uint8_t));
    if (!level_str)
        return DATABANK_MALLOC;
    snprintf(level_str, level_length, "%d", level);
    status = squash_codec_compress(codec, &compressed_size, compressed, text_len, text, "level", level_str, NULL);
    free(level_str);
    free(text);
    if (status != SQUASH_OK)
        return DATABANK_COMPRESS_ERROR;
    name = get_file_name(node_name);
    if (!name)
        return DATABANK_MALLOC;
    header = create_node_header(name, compressed_size, text_len, &header_length, &d_status);
    if (d_status != DATABANK_OK)
        return d_status;
    fwrite(header, sizeof(uint8_t), header_length, outfile);
    fwrite(compressed, sizeof(uint8_t), compressed_size, outfile);
    *compressed_len = compressed_size;
    *uncompressed_len = text_len;
    free(name);
    free(compressed);
    free(header);
    return DATABANK_OK;
}

int write_header(t_databank_options *options, FILE *file) {
    uint8_t *header;
    uint64_t header_len;
    int64_t file_lengths;

    file_lengths = get_file_lengths((const char **) options->files, options->nodes);
    if (file_lengths < 0)
        return -1;
    options->file_sizes = (uint64_t) file_lengths;
    if (options->compression_algorithm != NULL)
        header_len = get_len_of_number(options->nodes) + strlen(options->compression_algorithm) +
                     get_len_of_number((uint64_t) file_lengths) + 5;
    else {
        header_len = (uint64_t) get_len_of_number(options->nodes) + get_len_of_number((uint64_t) file_lengths) + 9;
        options->compression_algorithm = strdup("NONE");
        if (!options->compression_algorithm)
            return -1;
    }
    header = databank_malloc(header_len * sizeof(uint8_t));
    if (!header)
        return -1;
#ifdef __linux__
    snprintf((char *) header, header_len, "\1%lu\23%s\23%ld\2", options->nodes, options->compression_algorithm,
             file_lengths);
#elif __APPLE__
    snprintf((char *)header, header_len, "\1%lld\23%s\23%lld\2", options->nodes, options->compression_algorithm, file_lengths);
#endif
    fwrite(header, sizeof(uint8_t), header_len - 1, file);
    free(header);
    return 0;
}

// @TODO FIX MEMORY
// @TODO Add support for compressing parts of a file for large files
static DatabankStatus create_compressed_bank(t_databank_options *options, FILE *outfile) {
    DatabankStatus status;
    uint64_t compressed;
    uint64_t uncompressed;
    uint64_t total_uncompressed_bytes;
    uint64_t total_compressed_bytes;
    clock_t start_time;
    int i;

    start_time = clock();
    if (write_header(options, outfile) != 0)
        return -1;
    total_compressed_bytes = 0;
    total_uncompressed_bytes = 0;
    for (i = 0; options->files[i]; i++) {
        status = write_node(options->files[i], options->codec, outfile, options->level, &compressed, &uncompressed);
        if (status != DATABANK_OK)
            return status;
        if (DATABANK_VERBOSE)
            printf("File: %s\tCompression Ratio: %f\n", options->files[i], (double) uncompressed / (double) compressed);
        total_compressed_bytes += compressed;
        total_uncompressed_bytes += uncompressed;
    }
#ifdef __APPLE__
    printf("Total compressed bytes: %llu Total uncompressed bytes: %lld Compression Ratio: %f\n", total_compressed_bytes,
            total_uncompressed_bytes, (double)total_uncompressed_bytes / (double)total_compressed_bytes);
#elif __linux__
    printf("Total compressed bytes: %lu Total uncompressed bytes: %ld Compression Ratio: %f\n", total_compressed_bytes,
           total_uncompressed_bytes, (double) total_uncompressed_bytes / (double) total_compressed_bytes);
#endif
    printf("Time took %f\n", (clock() - start_time) / (double) CLOCKS_PER_SEC);
    printf("MegaBytes Per Second: %f\n",
           (total_compressed_bytes / ((clock() - start_time) / (double) CLOCKS_PER_SEC)) / 1000000.0);
    return DATABANK_OK;
}

DatabankStatus create_bank(t_databank_options *options) {
    FILE *outfile;
    DatabankStatus status = 0;

    outfile = fopen(options->outfile, "wb");
    if (!outfile) {
        fprintf(stderr, "Failed to open the file %s with error %d\n", options->outfile, errno);
        return -1;
    }
    status = create_compressed_bank(options, outfile);
    fclose(outfile);
    return status;
}