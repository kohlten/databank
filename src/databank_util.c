//
// Created by kohlten on 1/30/19.
//

#include "databank_util.h"

#include <stdio.h>
#include <stdlib.h>

void print_help() {
    char *message = "create_databank -a <ALGORITHM> -c -l LEVEL outfile FILENAMES...\n" \
                    "create_databank -d output_file input_file\n\n" \
                    "All files should be different names to access them as they will be put into the" \
                    " archive under their file name rather than their full path.\n\n" \
                    "Options:\n"\
                    "\t-a --algorithm what compression algorithm to use. Optional\n" \
                    "\t-h --help display this message\n" \
                    "\t-d --decompress decompress a compressed databank file\n" \
                    "\t-c --compress make a new compressed databank\n" \
                    "\t-l --level level to use for compression, if not provided will use the default. See below for valid numbers.\n" \
                    "Compression Algorithms with their range of compression levels:\n" \
                    "\t lzo1f:          1,999      lzo1c:          1-9,99,999 \n" \
                    "\t lzo1z:          999        lzo1y:          1,999      \n" \
                    "\t crush:          0-2        brotli:         1-11       \n" \
                    "\t lz4:            0-16       fastlz:         1-2        \n" \
                    "\t lz4-raw         0-14       lzf:            1,9        \n" \
                    "\t lzg:            1-9        lzham:          0-4        \n" \
                    "\t wflz:           1-2        lzma:           1-9        \n" \
                    "\t wflz-chunked:   1-2        lzfse           0          \n" \
                    "\t zlib:           1-9        fari:           0          \n" \
                    "\t gzip:           1-9        heatshrink      0          \n" \
                    "\t deflate:        1-9        brieflz:        0          \n" \
                    "\t zpaq:           1-5        gipfeli:        0          \n" \
                    "\t zstd:           1-22       lznt1:          0          \n" \
                    "\t zling:          0-4        xpress:         0          \n" \
                    "\t bzip2           1-9        xpress-huffman: 0          \n" \
                    "\t lzo1b:          1,99       snappy:         0          \n" \
                    "\t bsc:            0          yalz77          0          \n" \
                    "\t lzjb            0                                     \n";
    printf("%s", message);
}

int error(char *message, bool quit, bool help) {
    fprintf(stderr, "ERROR: %s\n", message);
    if (help)
        print_help();
    if (quit)
        exit(1);
    return -1;
}

int get_len_of_number(uint64_t n) {
    int len;

    len = 0;
    while (n > 0) {
        n /= 10;
        len++;
    }
    return len;
}