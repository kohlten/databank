//
// Created by kohlten on 2/4/19.
//

#include <stdio.h>
#include <time.h>
#include "databank_decompress_and_extract.h"

DatabankStatus decompress_and_extract_bank(t_databank_options *options) {
    t_databank databank;
    DatabankStatus status;
    hashtable_error err;
    t_hashtable_node **nodes;
    t_databank_node *databank_node;
    char *name;
    FILE *file;
    uint64_t node_lengths;
    uint64_t bytes;
    uint64_t i;
    clock_t start_time;

    start_time = clock();
    status = databank_init(&databank, options->files[0], NULL, NULL);
    if (status != DATABANK_OK)
        return status;
    err = get_items_hashtable(&databank.hashtable, &nodes, &node_lengths);
    if (err != HASH_SUCCESS) {
        databank_free(&databank);
        return DATABANK_HASHTABLE;
    }
    for (i = 0; i < node_lengths; i++) {
        if (nodes[i]) {
            databank_node = nodes[i]->data;
            name = nodes[i]->key;
            file = fopen(name, "wb");
            if (!file)
                return DATABANK_IO;
            bytes = fwrite(databank_node->data, sizeof(uint8_t), databank_node->uncompressed_size, file);
            if (bytes != databank_node->uncompressed_size) {
                fclose(file);
                return DATABANK_IO;
            }
            fclose(file);
        }
    }
    databank_free(&databank);
    free(nodes);
    if (DATABANK_VERBOSE)
        printf("DATABANK: MS to init: %f\n", ((clock() - start_time) / (double) CLOCKS_PER_SEC) * 1000);
    return DATABANK_OK;
}