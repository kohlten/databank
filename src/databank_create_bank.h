//
// Created by kohlten on 1/30/19.
//

#ifndef DATABANK_CREATE_BANK_H
#define DATABANK_CREATE_BANK_H

#include "databank_options.h"
#include "databank_status.h"

DatabankStatus create_bank(t_databank_options *options);

#endif //DATABANK_CREATE_BANK_H
