//
// Created by kohlten on 1/30/19.
//

#ifndef DATABANK_OPTIONS_H
#define DATABANK_OPTIONS_H

#include <stdint.h>
#include <squash-0.8/squash.h>

#define DATABANK_VERBOSE 1

typedef enum {
    DATABANK_COMPRESS = 1 << 0,
    DATABANK_DECOMPRESS = 1 << 1,
} DATABANK_OPERATION_TYPE;

typedef struct s_databank_options {
    char *outfile;
    char **files;
    char *compression_algorithm;
    SquashCodec *codec;
    DATABANK_OPERATION_TYPE type;
    uint64_t nodes;
    uint32_t level;
    uint64_t file_sizes;
} t_databank_options;

void free_options(t_databank_options *options);

int parse_options(int argc, char **argv, t_databank_options *options);

#endif //DATABANK_H
