/** @addtogroup databank
 * @{
*/

//
// Created by kohlten on 2/3/19.
//

#include "databank_status.h"

/**
 * Returns the string version of a error.
 * @param status The current status
 * @return String version of error
 */
const char *get_databank_status_message(DatabankStatus status) {
    switch (status) {
        case DATABANK_COMPRESS_ERROR:
            return "Failed to compress data.";
        case DATABANK_HASHTABLE:
            return "Error hashing.";
        case DATABANK_HASHTABLE_NOT_FOUND:
            return "File was not found in the archive.";
        case DATABANK_MALLOC:
            return "Failed to malloc, most likely out of memory.";
        case DATABANK_INVALID_ARCHIVE:
            return "Archive is corrupted.";
        case DATABANK_OPTIONS:
            return "Inputted invalid options.";
        case DATABANK_IO:
            return "Failed to read or write to a file.";
        case DATABANK_INVALID_HEADER:
            return "Failed to parse the header.";
        default:
            return "Unknown status.";
    }
}

/**
* @}
*/