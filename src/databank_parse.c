/** @defgroup databank
 * @{
*/
//
// Created by kohlten on 2/3/19.
//

#include "databank_parse.h"
#include "databank_io.h"
#include "databank_memory.h"
#include "databank_util.h"
#include "databank_options.h"

#include <stdio.h>
#include <string.h>
#include <time.h>
#include <ctype.h>

static inline int check_zone(uint8_t **zone, int (*cmp)(int), char c, uint64_t *loc, uint64_t zone_size) {
    while (*loc <= zone_size && **zone != c) {
        if (cmp)
            if (!cmp(**zone))
                break;
        *loc += 1;
        *zone += 1;
    }
    if (**zone != c)
        return -1;
    *loc += 1;
    return 0;
}

static inline t_databank_header *parse_header(uint8_t *zone, uint64_t zone_size) {
    t_databank_header *header;
    uint64_t start;

    if (!zone)
        return NULL;
    header = databank_malloc(sizeof(t_databank_header));
    if (!header)
        return NULL;
    header->header_size = 1;
    if (zone[0] != '\1') {
        free(header);
        return NULL;
    }
    zone += 1;
    start = header->header_size;
    if (check_zone(&zone, isdigit, '\23', &header->header_size, zone_size) != 0) {
        free(header);
        return NULL;
    }
    header->nodes = (uint64_t) atoll((char *) zone - ((header->header_size - start) - 1));
    header->header_size++;
    zone++;
    start = header->header_size;
    if (check_zone(&zone, NULL, '\23', &header->header_size, zone_size) != 0) {
        free(header);
        return NULL;
    }
    header->compression_algorithm = strndup((char *) zone - ((header->header_size - start) - 1),
                                            header->header_size - (start + 1));
    if (!header->compression_algorithm) {
        free(header);
        return NULL;
    }
    header->header_size++;
    zone++;
    start = header->header_size;
    if (check_zone(&zone, isdigit, '\2', &header->header_size, zone_size) != 0) {
        free(header);
        return NULL;
    }
    header->decompressed_size = (uint64_t) atoll((char *) zone - ((header->header_size - start) - 1));
    return header;
}

static inline int64_t check_and_parse_header(uint8_t *archive, uint64_t archive_len, t_databank_header **header) {
    uint64_t total_bytes;
    uint8_t *header_text;

    total_bytes = 0;
    while (archive[total_bytes] != '\2' && total_bytes < archive_len)
        total_bytes++;
    header_text = databank_malloc((total_bytes + 3) * sizeof(uint8_t));
    if (!header_text)
        return -1;
    memcpy(header_text, archive, total_bytes + 1);
    *header = parse_header(header_text, total_bytes + 1);
    free(header_text);
    if (!*header)
        return error("Malformed header!", false, false);
    return total_bytes + 1;
}

// @TODO Add better header checks
static inline DatabankStatus
parse_node(uint8_t *zone, uint64_t zone_length, t_databank_node **node, SquashCodec *codec, uint64_t *bytes) {
    uint64_t i;
    uint64_t start;
    char *type;
    uint8_t *decompressed;
    uint64_t decompressed_size;
    SquashStatus status;

    if (zone[0] != '\2')
        return DATABANK_INVALID_ARCHIVE;
    i = 1;
    start = i;
    *node = databank_malloc(sizeof(t_databank_node));
    if (!*node)
        return DATABANK_MALLOC;
    while (zone[i] != '\23' && i < zone_length)
        i++;
    if (i - start > 50)
        return DATABANK_INVALID_ARCHIVE;
    (*node)->name = strndup((char *) zone + start, i - start);
    if (!(*node)->name)
        return DATABANK_MALLOC;
    i++;
    start = i;
    while (zone[i] != '\23' && i < zone_length)
        i++;
    if (i - start > 50)
        return DATABANK_INVALID_ARCHIVE;
    type = strndup((char *) zone + start, i - start);
    if (!type)
        return DATABANK_MALLOC;
    (*node)->type = type_of_file(type);
    free(type);
    i++;
    start = i;
    while (zone[i] != '\23' && i < zone_length)
        i++;
    if (i - start > 50)
        return DATABANK_INVALID_ARCHIVE;
    (*node)->compressed_size = (uint64_t) atoll((char *) zone + start);
    i++;
    start = i;
    while (zone[i] != '\23' && i < zone_length)
        i++;
    if (i - start > 50)
        return DATABANK_INVALID_ARCHIVE;
    (*node)->uncompressed_size = (uint64_t) atoll((char *) zone + start);
    i++;
    decompressed_size = (*node)->uncompressed_size * 100;
    decompressed = databank_malloc(decompressed_size * sizeof(uint8_t));
    if (!decompressed)
        return DATABANK_MALLOC;
    status = squash_codec_decompress(codec, &decompressed_size, decompressed, (*node)->compressed_size, zone + i, NULL);
    if (status != SQUASH_OK)
        return -3;
    (*node)->data = decompressed;
    *bytes = i + (*node)->compressed_size;
    return DATABANK_OK;
}

static DatabankStatus
parse_zones(t_databank *databank, t_databank_header *header, uint8_t *archive, uint64_t size, SquashCodec *codec) {
    uint64_t nodes;
    uint64_t bytes_processed;
    uint64_t bytes;
    DatabankStatus status;
    t_databank_node *node;

    nodes = 0;
    bytes_processed = 0;
    node = NULL;
    while (nodes < header->nodes) {
        status = parse_node(archive + bytes_processed, size, &node, codec, &bytes);
        if (status != DATABANK_OK)
            return status;
        if (add_node_hashtable(&databank->hashtable, node->name, node) != HASH_SUCCESS)
            return DATABANK_HASHTABLE;
        node = NULL;
        bytes_processed += bytes;
        nodes++;
        if (bytes_processed > size)
            return DATABANK_INVALID_ARCHIVE;
    }
    return DATABANK_OK;
}

// @TODO Fix memory
static DatabankStatus parse_archive(t_databank *databank, const char *archive_name) {
    t_databank_header *header;
    SquashCodec *codec;
    uint64_t archive_len;
    int64_t bytes;
    uint8_t *archive;
    DatabankStatus err;

    archive = (uint8_t *) read_binary_file(archive_name, &archive_len);
    if (!archive)
        return DATABANK_IO;
    bytes = check_and_parse_header(archive, archive_len, &header);
    if (bytes <= 0)
        return DATABANK_INVALID_HEADER;
    codec = squash_get_codec(header->compression_algorithm);
    if (!codec)
        return DATABANK_INVALID_HEADER;
    err = parse_zones(databank, header, archive + bytes, archive_len - bytes, codec);
    free(header->compression_algorithm);
    free(header);
    free(archive);
    return err;
}

/**
 * Takes the databank and the archive and reads the data from the archive putting it into a hashtable.
 * Nodes will be available under their names without the path.
 * @param databank Databank struct pointer
 * @param archive_name Name of the file that contains the archive
 * @param image_load Function that loads a image for use, NULL can be put. See databankload.
 * @param font_load Function that loads a font for use, NULL can be put. See databankload.
 * @param buffer_size The size to read from the file at a time. Larger is better, but will cost more memory.
 * @return DatabankStatus denoting if there was a error or not. See DatabankStatus
 */
DatabankStatus
databank_init(t_databank *databank, const char *archive_name, DatabankLoad image_load, DatabankLoad font_load) {
    DatabankStatus err;
    clock_t start;

    if (!databank)
        return DATABANK_OPTIONS;
    if (!archive_name)
        return DATABANK_INVALID_ARCHIVE;
    start = clock();
    if (init_hashtable(&databank->hashtable, 0) != HASH_SUCCESS)
        return DATABANK_HASHTABLE;
    databank->load_image = image_load;
    databank->load_font = font_load;
    err = parse_archive(databank, archive_name);
    if (DATABANK_VERBOSE)
        printf("DATABANK: MS to init: %f\n", ((clock() - start) / (double) CLOCKS_PER_SEC) * 1000);
    return err;
}

/**
 * Gets the file from the archive and returns it.
 * If it is a image, or a font, it will check if you provided a function.
 * If so, it will call that function and return the data from that function to you.
 * It will then assign the data_length, the type to those.
 * If there is an error it will assign it to err.
 * See DatabankStatus for more details on the errors.
 *
 * It is up to the user to free the outputted data.
 * @param databank Databank struct pointer
 * @param name Filename to get from the archive
 * @param data_length Pointer to a uint64 that will be assigned to the length of the output data
 * @param type DatabankType pointer. Will assign the type of the file to this pointer.
 * @param err If there is an error, it will be stored inside of this.
 * @return The output of load_image, load_font or a copy of the data.
 */
void *
databank_load(t_databank *databank, const char *name, uint64_t *data_length, DatabankType *type, DatabankStatus *err) {
    t_databank_node *node;
    hashtable_error hash_err;
    void *data;
    clock_t start;

    if (!err)
        return NULL;
    if (!databank || !name || !type) {
        *err = DATABANK_OPTIONS;
        return (NULL);
    }
    start = clock();
    hash_err = get_node_hashtable(&databank->hashtable, name, (void **) &node);
    if (hash_err == HASH_NOT_FOUND) {
        *err = DATABANK_HASHTABLE_NOT_FOUND;
        return (NULL);
    } else if (hash_err != HASH_SUCCESS) {
        *err = DATABANK_HASHTABLE;
        return (NULL);
    }
    if (node->type == DATABANK_IMAGE && databank->load_image)
        data = databank->load_image(node->data, node->uncompressed_size);
    else if (node->type == DATABANK_FONT && databank->load_font)
        data = databank->load_font(node->data, node->uncompressed_size);
    else
        data = databank_memcopy(node->data, node->uncompressed_size);
    if (!data) {
        *err = DATABANK_MALLOC;
        return (NULL);
    }
    if (data_length)
        *data_length = node->uncompressed_size;
    *type = node->type;
    *err = DATABANK_OK;
    if (DATABANK_VERBOSE)
        printf("DATABANK: MS to lookup: %f\n", ((clock() - start) / (double) CLOCKS_PER_SEC) * 1000);
    return data;
}

static void databank_free_node(void *p) {
    t_databank_node *node;

    node = (t_databank_node *) p;
    if (node->name)
        free(node->name);
    if (node->data)
        free(node->data);
    free(node);
}

/**
 * Frees the databank and the internal hashtable
 * @param databank Pointer to the databank struct
*/
void databank_free(t_databank *databank) {
    free_hashtable(&databank->hashtable, true, databank_free_node);
}

/**
 * @}
*/