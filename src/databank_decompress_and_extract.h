//
// Created by kohlten on 2/4/19.
//

#ifndef DATABANK_DECOMPRESS_AND_EXTRACT_H
#define DATABANK_DECOMPRESS_AND_EXTRACT_H

#include "databank_parse.h"
#include "databank_options.h"

DatabankStatus decompress_and_extract_bank(t_databank_options *options);

#endif //DATABANK_DECOMPRESS_AND_EXTRACT_H
