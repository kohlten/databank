#include "databank_options.h"
#include "databank_memory.h"
#include "databank_create_bank.h"
#include "databank_util.h"
#include "databank_decompress_and_extract.h"
#include "databank_status.h"

#include <strings.h>

// @TODO Add documentation
// @TODO Add hash for each node
int main(int argc, char **argv) {
    t_databank_options options;
    DatabankStatus status;

    bzero(&options, sizeof(t_databank_options));
    if (parse_options(argc, argv, &options) != 0) {
        free_options(&options);
        return 1;
    }
    if ((options.type & DATABANK_COMPRESS) > 0 || options.type == 0) {
        status = create_bank(&options);
        if (status != DATABANK_OK) {
            fprintf(stderr, "ERROR: Failed to create databank. Error message: %s\n", get_databank_status_message(status));
            free_options(&options);
            return 1;
        }
    } else {
        status = decompress_and_extract_bank(&options);
        if (status != DATABANK_OK) {
            fprintf(stderr, "ERROR: Failed to decompress and extract. Error message: %s\n", get_databank_status_message(status));
            free_options(&options);
            return 1;
        }
    }
    free_options(&options);
    return 0;
}