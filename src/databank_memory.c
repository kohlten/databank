//
// Created by kohlten on 1/30/19.
//

#include "databank_memory.h"

#include <string.h>

char **copy_2d_array(char **array, int len) {
    int i;
    char **new_array;

    new_array = malloc((len + 1) * sizeof(char *));
    if (!new_array)
        return NULL;
    for (i = 0; i < len; i++) {
        new_array[i] = strdup(array[i]);
        if (!new_array[i]) {
            free(new_array);
            return NULL;
        }
    }
    new_array[len] = NULL;
    return new_array;
}

void free_2d_array(char **array) {
    int i;

    for (i = 0; array[i]; i++)
        free(array[i]);
    free(array);
}

void *databank_malloc(size_t size) {
    void *data;

    data = malloc(size);
    if (!data)
        return (NULL);
    memset(data, 0, size);
    return data;
}

void *databank_memcopy(void *mem, size_t size) {
    void *new;

    new = databank_malloc(size);
    if (!new)
        return (NULL);
    memcpy(new, mem, size);
    return new;
}