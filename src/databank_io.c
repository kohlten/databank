//
// Created by kohlten on 1/30/19.
//

#include <errno.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

#include <openssl/md5.h>

#include "databank_io.h"
#include "databank_memory.h"

char *read_binary_file(const char *name, uint64_t *len) {
    FILE *f;
    char *string;
    uint64_t fsize;
    uint64_t read;

    f = fopen(name, "rb");
    if (!f) {
        fprintf(stderr, "Failed to open the file %s with error %d\n", name, errno);
        return NULL;
    }
    fseek(f, 0, SEEK_END);
    fsize = (uint64_t) ftell(f);
    fseek(f, 0, SEEK_SET);
    string = databank_malloc((fsize + 1) * sizeof(char));
    read = fread(string, sizeof(char), fsize, f);
    if (read != fsize) {
        free(string);
        return (NULL);
    }
    fclose(f);
    string[fsize] = 0;
    *len = fsize;
    return string;
}

int64_t get_file_lengths(const char **names, uint64_t len) {
    FILE *f;
    uint64_t i;
    uint64_t length;

    length = 0;
    for (i = 0; i < len; i++) {
        f = fopen(names[i], "rb");
        if (!f) {
            fprintf(stderr, "Failed to open the file %s with error %d\n", names[i], errno);
            return -1;
        }
        fseek(f, 0, SEEK_END);
        length += ftell(f);
        fclose(f);
    }
    return (int64_t) length;
}

// @TODO If a file has no type, and a path, it will return the whole file
char *get_file_type(const char *filename) {
    int64_t i;
    char *type;

    i = strlen(filename) - 1;
    for (; i >= 0; i--) {
        if (filename[i] == '.')
            break;
    }
    if (i == 0)
        return NULL;
    type = strdup((filename + i) + 1);
    return type;
}

char *get_file_name(const char *filename) {
    int64_t i;
    char *name;

    i = (int64_t) strlen(filename) - 1;
    for (; i >= 0; i--) {
        if (filename[i] == '\\' || filename[i] == '/')
            break;
    }
    name = strdup((filename + i) + 1);
    return name;
}

bool file_exists(const char *filename) {
    if (access(filename, F_OK | R_OK) == -1)
        return false;
    return true;
}

bool is_directory(const char *filename) {
    FILE *f;

    f = fopen(filename, "r+");
    if (f) {
        fclose(f);
        return false;
    }
    return errno == EISDIR;
}