/** @addtogroup databank
 * @{
*/
//
// Created by kohlten on 2/3/19.
//

#ifndef DATABANK_FILE_TYPE_H
#define DATABANK_FILE_TYPE_H

typedef enum {
    /** It's an image (png) */
            DATABANK_IMAGE,
    /** It's a font (ttf) */
            DATABANK_FONT,
    /** Unknown type */
            DATABANK_OTHER
} DatabankType;

/**
 * Return the type of file
 * @param type Extention of the file
 * @return DatabankType
 */
DatabankType type_of_file(char *type);

#endif //DATABANK_FILE_TYPE_H

/**
 * @}
*/