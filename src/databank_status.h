/** @addtogroup databank
 * @{
*/

//
// Created by kohlten on 2/3/19.
//

#ifndef DATABANK_STATUS_H
#define DATABANK_STATUS_H

typedef enum {
    /** Error hashing. */
            DATABANK_HASHTABLE,
    /** File was not found in the archive.*/
            DATABANK_HASHTABLE_NOT_FOUND,
    /** Failed to malloc, most likely out of memory. */
            DATABANK_MALLOC,
    /** Archive is corrupted. */
            DATABANK_INVALID_ARCHIVE,
    /** Inputted invalid options. */
            DATABANK_OPTIONS,
    /** Failed to read or write to a file. */
            DATABANK_IO,
    /** Failed to parse the header */
            DATABANK_INVALID_HEADER,
    /** Failed to compress data */
            DATABANK_COMPRESS_ERROR,
    /** We are good. */
            DATABANK_OK,
} DatabankStatus;

/**
 * Returns the string version of a error.
 * @param status The current status
 * @return String version of error
 */
const char *get_databank_status_message(DatabankStatus status);

#endif //DATABANK_STATUS_H

/**
* @}
*/