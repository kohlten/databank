//
// Created by kohlten on 1/30/19.
//

#ifndef DATABANK_UTIL_H
#define DATABANK_UTIL_H

#include <stdbool.h>
#include <stdint.h>

void print_help();

int error(char *message, bool quit, bool help);

int get_len_of_number(uint64_t n);

#endif //DATABANK_UTIL_H
