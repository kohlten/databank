//
// Created by kohlten on 2/3/19.
//

#ifndef DATABANK_PARSE_H
#define DATABANK_PARSE_H

#include "databank_hashtable.h"
#include "databank_file_type.h"
#include "databank_status.h"

#include <stdlib.h>
#include <stdint.h>

typedef void *(*DatabankLoad)(void *data, size_t data_size);

typedef struct s_databank_header {
    uint64_t nodes;
    uint64_t decompressed_size;
    uint64_t header_size;
    char *compression_algorithm;

} t_databank_header;

typedef struct s_databank_node {
    DatabankType type;
    char *name;
    char *hash;
    void *data;
    uint64_t compressed_size;
    uint64_t uncompressed_size;
} t_databank_node;

typedef struct s_databank {
    t_hashtable hashtable;
    DatabankLoad load_image;
    DatabankLoad load_font;
} t_databank;

DatabankStatus
databank_init(t_databank *databank, const char *archive_name, DatabankLoad image_load, DatabankLoad font_load);

void *
databank_load(t_databank *databank, const char *name, uint64_t *data_length, DatabankType *type, DatabankStatus *err);

void databank_free(t_databank *databank);

#endif //DATABANK_PARSE_H
