//
// Created by kohlten on 1/30/19.
//

#ifndef DATABANK_MEMORY_H
#define DATABANK_MEMORY_H

#include <stdlib.h>

char **copy_2d_array(char **array, int len);

void free_2d_array(char **array);

void *databank_malloc(size_t size);

void *databank_memcopy(void *mem, size_t size);


#endif //DATABANK_MEMORY_H
