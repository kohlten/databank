//
// Created by kohlten on 1/30/19.
//

#ifndef DATABANK_IO_H
#define DATABANK_IO_H

#include <stdint.h>
#include <stdbool.h>

char *read_binary_file(const char *name, uint64_t *len);

char *get_file_type(const char *filename);

int64_t get_file_lengths(const char **names, uint64_t len);

char *get_file_name(const char *filename);

bool file_exists(const char *filename);

bool is_directory(const char *filename);


#endif //DATABANK_IO_H
