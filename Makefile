OUT = build/create_databank
LIB = build/libdatabank.a

CFILES = src/databank_status.c \
	src/databank_options.c \
	src/databank_io.c \
	src/databank_hashtable.c \
	src/databank_util.c \
	src/databank_create_bank.c \
	src/databank_parse.c \
	src/databank_memory.c \
	src/databank_file_type.c \
	src/databank_decompress_and_extract.c

OFILES = $(patsubst src/%.c,build/obj/%.o,$(CFILES))

SQUASH_LIBS = $(shell pkg-config squash-0.8 --libs 2>/dev/null)

SQUASH_FLAGS = $(shell pkg-config squash-0.8 --cflags)

CC = gcc

ifeq ($(shell uname), Darwin)
PLATFORM_DEPENDENCIES =
endif
ifeq ($(shell uname), Linux)
PLATFORM_DEPENDENCIES =
endif

LIBS = $(SQUASH_LIBS)
CFLAGS =-g -Wall -Wextra -I src $(SQUASH_FLAGS) -O3

all: check build dependencies $(OFILES)
	@ar rc $(LIB) $(OFILES)
	@ranlib $(LIB)
	$(CC) -o $(OUT) $(PLATFORM_DEPENDENCIES) src/main.c $(OFILES) $(CFLAGS) $(LIBS)
	cp $(OUT) .

build_test: all
	@cd tests && make

check:
	@[ -z "$(SQUASH_LIBS)" ] && echo "libsquash not found on your system!" && exit 1 || exit 0

build/obj/%.o: src/%.c
	@echo "CC $(CFLAGS) -c -o build/obj/$(notdir $@) $<"
	@$(CC) $(CFLAGS) -c -o build/obj/$(notdir $@) $<

build:
	-@mkdir build
	-@mkdir build/obj

clean:
	-@rm -rf build/obj
	-@rm -rf $(OUT)

fclean: clean
	-@rm -rf build

re: clean all

.PHONY: all fclean clean re test dependencies build check
