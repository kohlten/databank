Databank

Compressed storage for game files. With Databank, you can put all of your game files into a single file which can then be used to import the files into your game.

See tests/front_end for an example.

Dependencies:

    gcc
    make or meson
    squash-0.8 https://github.com/quixdb/squash.git

You only need to use make or meson, not both.

Compiling:

    Squash:
        git clone https://github.com/quixdb/squash.git
        cd squash
        sh autogen.sh
        make
        sudo make install
        cd ..

    Make:
        make
        cp build/create_databank .
        ./create_databank

    Meson:
        meson build
        cd build
        ninja
        cp create_databank ..
        cd ..
        ./create_databank

Help:

    create_databank -a <ALGORITHM> -c -l LEVEL outfile FILENAMES...
    create_databank -d output_file input_file

    All files should be different names to access them as they will be put into the archive under their file name rather than their full path.

    Options:
        -a --algorithm what compression algorithm to use. Optional
        -h --help display this message
        -d --decompress decompress a compressed databank file
        -c --compress make a new compressed databank
        -l --level level to use for compression, if not provided will use the default. See below for valid numbers.
    Compression Algorithms with their range of compression levels:
         lzo1f:          1,999      lzo1c:          1-9,99,999
         lzo1z:          999        lzo1y:          1,999
         crush:          0-2        brotli:         1-11
         lz4:            0-16       fastlz:         1-2
         lz4-raw         0-14       lzf:            1,9
         lzg:            1-9        lzham:          0-4
         wflz:           1-2        lzma:           1-9
         wflz-chunked:   1-2        lzfse           0
         zlib:           1-9        fari:           0
         gzip:           1-9        heatshrink      0
         deflate:        1-9        brieflz:        0
         zpaq:           1-5        gipfeli:        0
         zstd:           1-22       lznt1:          0
         zling:          0-4        xpress:         0
         bzip2           1-9        xpress-huffman: 0
         lzo1b:          1,99
