from subprocess import Popen, PIPE
from os         import listdir
from sys        import argv, exit
from random		import choice
from datetime   import datetime
from shutil     import copyfile
from time		import time

algorithms = ['lzo1b -l 99',  'lzo1c -l 999',
'lzo1f -l 999',  'lzo1y -l 999',
'lzo1z -l 999',  'brotli -l 11',
'crush -l 2',  'copy',
'fastlz -l 2',  'lz4 -l 16',
'lz4-raw -l 14',  'lzf -l 9',
'lzg -l 9',  'lzham -l 4',
'lzma -l 9',  'wflz -l 2',
'wflz-chunked -l 2',
'zlib -l 9',  'gzip -l 9',
'deflate -l 9',  'zpaq -l 5',
'zstd -l 22',  'zling -l 4',
'lzfse',  'fari',
'heatshrink',  'brieflz',
'gipfeli',  'lznt1',
'xpress',  'xpress-huffman',
'compress',  'quicklz',
'snappy',  'bsc',
'yalz77',  'lzjb',
'bzip2 -l 9']

'''algorithms = ['lzo1b',  'lzo1c',
'lzo1f',  'lzo1y',
'lzo1z',  'brotli',
'crush',  'copy',
'fastlz',  'lz4',
'lz4-raw',  'lzf',
'lzg',  'lzham',
'lzma',  'wflz',
'wflz-chunked',
'zlib',  'gzip',
'deflate',  'zpaq',
'zstd',  'zling',
'lzfse',  'fari',
'heatshrink',  'brieflz',
'gipfeli',  'lznt1',
'xpress',  'xpress-huffman',
'compress',  'quicklz',
'snappy',  'bsc',
'yalz77',  'lzjb',
'bzip2']'''

algorithm = None
ratio = None
speed = None

def run_one(dataset, logger, data):
	global algorithm, ratio, speed

	command = "./create_databank -a " + algorithm.split(" ")[0]
	p = Popen(command.split(" "), stderr=PIPE, stdout=PIPE)
	p.wait()
	out = p.communicate()
	if out[1] != "" or p.returncode != 0:
		print("Got a return code of " + str(p.returncode) + " and a output of " + out[1])
		return -1
	logger.write(out[0])
	command = "./create_databank -a " + algorithm + " -c " + algorithm.split(" ")[0] + ".comp " + ' '.join(dataset)
	p = Popen(command.split(" "),  stderr=PIPE, stdout=PIPE)
	out = p.communicate()
	if out[1] != "" or p.returncode != 0:
		print("Failed to compress " + str(p.returncode) + " and a output of " + out[1])
		return -1
	print(out[0])
	s = out[0].split()
	ratio = float(s[s.index("Ratio:") + 1])
	speed = float(s[s.index("Second:") + 1])
	logger.write("Ratio: " + str(ratio) + " Speed: " + str(speed) + '\n')
	command = "./create_databank -d " + algorithm.split(" ")[0] + ".comp"
	p = Popen(command.split(" "),  stderr=PIPE, stdout=PIPE)
	out = p.communicate()
	if out[1] != "" or p.returncode != 0:
		print("Failed to decomp " + str(p.returncode) + " and a output of " + out[1])
		return -1
	for i in range(len(dataset)):
		with open(dataset[i], 'r') as file:
			new_data = file.read()
		if new_data != data[i]:
			logger.write(new_data)
			logger.write(data)
			logger.write("Failed, data was not equal!")
			return -1
	return 0

def main():
	global algorithm, ratio, speed

	if len(argv) < 3:
		print("python data_test.py n datasets...")
		return 1
	try:
		n = int(argv[1])
	except:
		print(argv[1] + " could not be converted to an int!")
		return 1
	copyfile("../../build/create_databank", "./create_databank")
	dataset = argv[2:]
	dataset_data = []
	for filename in dataset:
		with open(filename, 'r') as data:
			dataset_data.append(data.read())
	logger = open(datetime.now().strftime("%m_%d_%Y_%H_%M_%S.log"), 'w')
	greatest_time = 0
	greatest_algorithm = None
	least_algorithm = None
	least_time = 0
	best_ratio_algorithm = None
	best_ratio = 0
	worst_ratio_algorithm = None
	worst_ratio = 0
	best_speed_algorithm = None
	best_speed = 0
	worst_speed_alogorithm = None
	worst_speed = 0
	algorithm_num = 35
	for i in range(n):
		algorithm = algorithms[algorithm_num]
		print(algorithm)
		start_time = time()
		if run_one(dataset, logger, dataset_data) != 0:
			return 1
		if time() - start_time < least_time or least_time == 0:
			least_time = time() - start_time
			least_algorithm = algorithm
		if time() - start_time > greatest_time or greatest_time == 0:
			greatest_time = time() - start_time
			greatest_algorithm = algorithm
		if ratio > best_ratio or best_ratio == 0:
			best_ratio = ratio
			best_ratio_algorithm = algorithm
		if ratio < worst_ratio or worst_ratio == 0:
			worst_ratio = ratio
			worst_ratio_algorithm = algorithm
		if speed > best_speed or best_speed == 0:
			best_speed = speed
			best_speed_algorithm = algorithm
		if speed < worst_speed or worst_speed == 0:
			worst_speed = speed
			worst_speed_alogorithm = algorithm
		algorithm_num += 1
		if algorithm_num >= len(algorithms):
			algorithm_num = 0
	logger.write("Greatest Time: " + greatest_algorithm + " Time: " + str(greatest_time) + '\n')
	logger.write("Least Time: " + least_algorithm + " Time: " + str(least_time) + '\n')
	logger.write("Greatest ratio: " + best_ratio_algorithm + " Ratio: " + str(best_ratio) + '\n')
	logger.write("Worst ratio: " + worst_ratio_algorithm + " Ratio: " + str(worst_ratio) + '\n')
	logger.write("Greatest speed: " + best_speed_algorithm + " Speed: " + str(best_speed) + '\n')
	logger.write("Worst speed: " + worst_speed_alogorithm + " Speed: " + str(worst_speed) + '\n')
	logger.close()

if __name__ == '__main__':
	exit(main())