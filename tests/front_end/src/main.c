//
// Created by Alexander Strole on 2/3/19.
//

#include "databank_parse.h"
#include "fps.h"

#include <SDL.h>
#include <SDL_image.h>
#include <stdio.h>

int init_sdl2(SDL_Window **window, SDL_Renderer **renderer, int width, int height) {
    if (SDL_Init(SDL_INIT_VIDEO) < 0) {
        printf("SDL2 Failed! %s\n", SDL_GetError());
        return -1;
    }
    *window = SDL_CreateWindow("Test", 0, 0, width, height, 0);
    if (!*window)
        return -1;
    *renderer = SDL_CreateRenderer(*window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
    if (!*renderer)
        return -1;
    if (!(IMG_Init(IMG_INIT_PNG) & IMG_INIT_PNG))
        return -1;
    return 0;
}

static void *load_image(void *data, size_t data_len) {
    SDL_RWops *ops;
    SDL_Surface *out;
    SDL_Surface *surf;

    ops = SDL_RWFromMem(data, data_len);
    if (!ops) {
        printf("ERROR: Failed to open data! SDL_ERROR: %s\n", SDL_GetError());
        return NULL;
    }
    surf = IMG_Load_RW(ops, 0);
    SDL_RWclose(ops);
    if (!surf) {
        printf("ERROR: Failed to parse image! SDL_ERROR: %s\n", SDL_GetError());
        return NULL;
    }
    out = SDL_ConvertSurface(surf, surf->format, 0);
    SDL_FreeSurface(surf);
    if (!out)
        return NULL;
    return out;
}

static int draw_surface(SDL_Surface *surface, SDL_Renderer *renderer, int x, int y, float zoom) {
    SDL_Texture *text = NULL;

    text = SDL_CreateTextureFromSurface(renderer, surface);
    if (!text)
        return -1;
    SDL_RenderCopy(renderer, text, NULL,
                   &(SDL_Rect) {x, y, surface->w * zoom, surface->h * zoom});
    SDL_DestroyTexture(text);
    return 0;
}


static int display_images(SDL_Renderer *renderer, t_databank *databank) {
    DatabankStatus err;
    DatabankType type;
    SDL_Surface *current;
    int i;
    int x;
    int y;
    float ratio = 0.40;
    char *images[] = {"e10.png", "e11.png", "e12.png",
                      "e13.png", "e14.png", "e15.png",
                      "e16.png", "e1.png", "e2.png",
                      "e3.png", "e4.png", "e5.png",
                      "e6.png","e7.png","e8.png",
                      "e9.png", NULL};

    x = 0;
    y = 0;
    for (i = 0; images[i]; i++) {
        current = databank_load(databank, images[i], NULL, &type, &err);
        if (err != DATABANK_OK || type != DATABANK_IMAGE) {
            if (err != DATABANK_OK)
                printf("Got an error: %d: %s\n", err, get_databank_status_message(err));
            else
                printf("Got %d when I expected %d\n", type, DATABANK_IMAGE);
            return -1;
        }
        draw_surface(current, renderer, x, y, ratio);
        SDL_FreeSurface(current);
        if (x >= 500 - (320 * ratio)) {
            x = 0;
            y += 240 * ratio;
        } else {
            x += 320 * ratio;
        }
    }
    return 0;
}

int main() {
    t_databank databank;
    SDL_Window *window;
    SDL_Renderer *renderer;
    DatabankStatus err;
    SDL_Event e;
    bool done;
    int frames;
    t_fps fps;
    int c_fps;

    done = false;
    frames = 0;
    err = databank_init(&databank, "out.data", load_image, NULL);
    if (err != DATABANK_OK) {
        printf("Failed!\n");
        return 1;
    }
    init_fps(&fps, 0);
    if (init_sdl2(&window, &renderer, 500, 500) != 0)
        return 1;
    while (!done && frames < 5) {
        while (SDL_PollEvent(&e)) {
            if (e.type == SDL_QUIT)
                done = true;
        }
        SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
        SDL_RenderClear(renderer);
        SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255);
        if (display_images(renderer, &databank) != 0)
            return 1;
        SDL_RenderPresent(renderer);
        c_fps = get_fps(&fps);
        if (c_fps > 0 && frames % 60 == 0)
            printf("FPS: %d\n", c_fps);
        update_fps(&fps);
        frames++;
    }
    IMG_Quit();
    SDL_Quit();
    databank_free(&databank);
    return 0;
}
